# XLEngine
## Project Overview
### Objective
I'd like for this project to slowly build into an actual engine over time, but right now I'm mostly using it to learn Vulkan.

[Video of its current state](https://www.youtube.com/watch?v=Bpr5UjPxkso)