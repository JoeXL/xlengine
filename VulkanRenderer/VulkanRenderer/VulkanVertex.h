#pragma once
#include "BaseVertex.h"

#include <vulkan/vulkan.h>

class VulkanVertex :
	public BaseVertex {
public:
	VulkanVertex();
	virtual ~VulkanVertex();

	static VkVertexInputBindingDescription getBindingDescription() {
		VkVertexInputBindingDescription bindingDescription = {};
		bindingDescription.binding = 0;
		bindingDescription.stride = sizeof(VulkanVertex);
		bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

		return bindingDescription;
	}

	static std::array<VkVertexInputAttributeDescription, 3> getAttributeDescriptions() {
		std::array<VkVertexInputAttributeDescription, 3> attributeDescriptions = {};
		/* Format notes:
			float = VK_FORMAT_R32_SFLOAT
			vec2  = VK_FROMAT_R32G32_SFLOAT
			vec3  = VK_FORMAT_R32G32B32_SFLOAT
			vec4  = VK_FORMAT_R32G32B32A32_SFLOAT
			ivec2 = VK_FORMAT_R32G32_SINT
			uvec4 = VK_FORMAT_R32G32B32A32_UINT
			double= VK_FORMAT_R64_SFLOAT */
		attributeDescriptions[0].binding = 0;
		attributeDescriptions[0].location = 0;
		attributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[0].offset = offsetof(VulkanVertex, pos);

		attributeDescriptions[1].binding = 0;
		attributeDescriptions[1].location = 1;
		attributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
		attributeDescriptions[1].offset = offsetof(VulkanVertex, color);

		attributeDescriptions[2].binding = 0;
		attributeDescriptions[2].location = 2;
		attributeDescriptions[2].format = VK_FORMAT_R32G32_SFLOAT;
		attributeDescriptions[2].offset = offsetof(VulkanVertex, texCoord);

		return attributeDescriptions;
	}

	bool operator==(const VulkanVertex& other) const {
		return pos == other.pos && color == other.color && texCoord == other.texCoord;
	}
};

namespace std {
	template<> struct hash<VulkanVertex> {
		size_t operator()(VulkanVertex const& vertex) const {
			return ((hash<glm::vec3>()(vertex.pos) ^
				(hash<glm::vec3>()(vertex.color) << 1)) >> 1) ^
					 (hash<glm::vec2>()(vertex.texCoord) << 1);
		}
	};
}