#pragma once

#include "BaseVertex.h"

class BaseModel {
public:
	BaseModel();
	virtual ~BaseModel();

	/*unsigned int getVertexCount() const;
	unsigned int getIndexCount() const;

	const std::vector<glm::vec3>& getPositionData() const;
	const std::vector<glm::vec2>& getTexCoordData() const;
	const std::vector<glm::vec4>& getColorData() const;
	const std::vector<glm::vec3>& getNormalData() const;
	const std::vector<glm::vec3>& getTangentData() const;
	const std::vector<unsigned int>& getIndexData() const;

	void setVertexPositions(const std::vector<glm::vec3>& newPos);
	void setVertexTexCoords(const std::vector<glm::vec2>& newTex);
	void setVertexColors(const std::vector<glm::vec4>& newColors);
	void setVertexNormals(const std::vector<glm::vec3>& newNormals);
	void setVertexTangents(const std::vector<glm::vec3>& newTangents);
	void setVertexIndices(const std::vector<unsigned int>& newIndices);

	void recalculateNormals();
	void recalculateTangents();

	glm::vec3 generateTangent(const glm::vec3& a, const glm::vec3& b,
							  const glm::vec3& c, const glm::vec2& ta,
							  const glm::vec2& tb, const glm::vec2& tc);*/

	virtual void uploadToGPU() = 0;

	virtual void loadFile(const std::string& filename, glm::vec3 scale);

	const std::vector<BaseVertex>& getVertices() const { return vertices; }
	const std::vector<unsigned int>& getIndices() const { return indices; }

protected:
	std::vector<BaseVertex> vertices;
	std::vector<unsigned int> indices;
};

