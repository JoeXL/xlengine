#pragma once
#include <string>

namespace XLEngine {
	namespace Assets {
		const std::string SHADER_PATH("../../../assets/shaders/");
		const std::string MODEL_PATH("../../../assets/models/");
		const std::string TEXTURE_PATH("../../../assets/textures/");
	}
}