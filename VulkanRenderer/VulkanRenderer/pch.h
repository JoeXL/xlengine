#ifndef PCH_H
#define PCH_H

#include "AssetPaths.h"

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/hash.hpp>

#include <iostream>
#include <fstream>
#include <stdexcept>
#include <algorithm>
#include <functional>
#include <chrono>
#include <cstdlib>
#include <vector>
#include <array>
#include <optional>
#include <set>
#include <unordered_map>
#include <memory>

#endif //PCH_H
