#pragma once
#include "BaseTexture.h"
#include "XLVulkanContext.h"

class VulkanTexture :
	public BaseTexture {
public:
	VulkanTexture(XLVulkanContext& context);
	virtual ~VulkanTexture();

	virtual void loadFile(const std::string& filename) override;

	VkImageView& getTextureImageView() { return textureImageView; }
	VkSampler& getTextureSampler() { return textureSampler; }

	uint32_t getMipLevels() { return mipLevels; }

	std::string id;

protected:
	void createTextureImage(unsigned char* pixels, int texWidth, int texHeight);
	void createTextureImageView();
	void createTextureSampler();

	XLVulkanContext& vk;

	uint32_t mipLevels;
	VkImage textureImage;
	VkDeviceMemory textureImageMemory;
	VkImageView textureImageView;
	VkSampler textureSampler;

	friend class XLVulkanContext;
};

