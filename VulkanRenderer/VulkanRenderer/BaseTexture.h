#pragma once
class BaseTexture {
public:
	BaseTexture();
	virtual ~BaseTexture();

	virtual void loadFile(const std::string& filename) = 0;
};

