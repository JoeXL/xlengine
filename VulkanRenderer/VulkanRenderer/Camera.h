#pragma once

enum CameraMovement {
	FORWARD,
	BACKWARD,
	LEFT,
	RIGHT,
	UP,
	DOWN
};

class Camera {
public:
	Camera();
	Camera(float pitch, float yaw, const glm::vec3& position);
	virtual ~Camera();

	const glm::vec3& getPosition() { return position; }
	void setPosition(const glm::vec3& position) { this->position = position; }

	float getYaw() { return yaw; }
	void setYaw(float yaw) {
		this->yaw = yaw;
		updateCameraVectors();
	}

	float getPitch() { return pitch; }
	void setPitch(float pitch) {
		this->pitch = pitch;
		updateCameraVectors();
	}

	glm::mat4 buildViewMatrix() const;
	glm::mat4 buildProjectonMatrix(float currentAspect = 1.0f) const;

	void processMovement(CameraMovement direction, float dt);
	void processRotation(float xOffset, float yOffset);

protected:
	float nearPlane;
	float farPlane;
	/*float left;
	float right;
	float top;
	float bottom;

	float fov;
	float yaw;
	float pitch;
	glm::vec3 position;*/

	void updateCameraVectors();

	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 up;
	glm::vec3 right;
	glm::vec3 worldUp;

	float yaw;
	float pitch;

	float movementSpeed;
	float mouseSensitivity;
	float fov;
};

