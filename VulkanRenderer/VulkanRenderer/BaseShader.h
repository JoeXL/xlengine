#pragma once

enum ShaderType {
	VERTEX,
	FRAGMENT
};

class BaseShader {
public:
	BaseShader();
	virtual ~BaseShader();
};

