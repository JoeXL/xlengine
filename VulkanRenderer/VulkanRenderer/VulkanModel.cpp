#include "pch.h"
#include "VulkanModel.h"


VulkanModel::VulkanModel(XLVulkanContext& context) : vk(context) {
}


VulkanModel::~VulkanModel() {
	vkDestroyBuffer(vk.device, vertexBuffer, nullptr);
	vkFreeMemory(vk.device, vertexBufferMemory, nullptr);

	vkDestroyBuffer(vk.device, indexBuffer, nullptr);
	vkFreeMemory(vk.device, indexBufferMemory, nullptr);
}

void VulkanModel::uploadToGPU() {
	createVertexBuffer();
	createIndexBuffer();
}

void VulkanModel::createVertexBuffer() {
	VkDeviceSize bufferSize = sizeof(vertices[0]) * vertices.size();

	VkBuffer stagingBuffer;
	VkDeviceMemory stagingBufferMemory;

	vk.createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
					| VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

	void* data;
	vkMapMemory(vk.getDevice(), stagingBufferMemory, 0, bufferSize, 0, &data);
	memcpy(data, vertices.data(), (size_t)bufferSize);
	vkUnmapMemory(vk.getDevice(), stagingBufferMemory);

	vk.createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
					VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, vertexBuffer, vertexBufferMemory);
	vk.copyBuffer(stagingBuffer, vertexBuffer, bufferSize);

	vkDestroyBuffer(vk.getDevice(), stagingBuffer, nullptr);
	vkFreeMemory(vk.getDevice(), stagingBufferMemory, nullptr);
}

void VulkanModel::createIndexBuffer() {
	VkDeviceSize bufferSize = sizeof(indices[0]) * indices.size();

	VkBuffer stagingBuffer;
	VkDeviceMemory stagingBufferMemory;
	vk.createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
				 | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, stagingBuffer, stagingBufferMemory);

	void* data;
	vkMapMemory(vk.getDevice(), stagingBufferMemory, 0, bufferSize, 0, &data);
	memcpy(data, indices.data(), (size_t)bufferSize);
	vkUnmapMemory(vk.getDevice(), stagingBufferMemory);

	vk.createBuffer(bufferSize, VK_BUFFER_USAGE_TRANSFER_DST_BIT | VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
				 VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, indexBuffer, indexBufferMemory);

	vk.copyBuffer(stagingBuffer, indexBuffer, bufferSize);

	vkDestroyBuffer(vk.getDevice(), stagingBuffer, nullptr);
	vkFreeMemory(vk.getDevice(), stagingBufferMemory, nullptr);
}