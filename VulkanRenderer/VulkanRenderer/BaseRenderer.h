#pragma once
#include "BaseWindow.h"

class BaseRenderer {
public:
	BaseRenderer(BaseWindow& w);
	virtual ~BaseRenderer();

	virtual void init() = 0;

	void render();

	bool shouldClose = false;

protected:
	virtual void beginFrame() = 0;
	virtual void renderFrame() = 0;
	virtual void endFrame() = 0;

	BaseWindow& window;
};

