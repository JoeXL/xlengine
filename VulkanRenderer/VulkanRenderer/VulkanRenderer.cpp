#include "pch.h"
#include "VulkanRenderer.h"

#include "VulkanVertex.h"

VulkanRenderer::VulkanRenderer(VulkanWindow& w) : BaseRenderer(w), vk(w) {
}


VulkanRenderer::~VulkanRenderer() {
	models.clear();
	textures.clear();
	root->cleanup();
	root = nullptr;
}

void VulkanRenderer::beginFrame() {

}

void VulkanRenderer::renderFrame() {
	vkWaitForFences(vk.device, 1, &vk.inFlightFences[vk.currentFrame], VK_TRUE, std::numeric_limits<uint64_t>::max());

	uint32_t imageIndex;
	VkResult result = vkAcquireNextImageKHR(vk.device, vk.swapChain, std::numeric_limits<uint64_t>::max(),
											vk.imageAvailableSemaphores[vk.currentFrame], VK_NULL_HANDLE, &imageIndex);

	if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR || window.hasResized()) {
		window.setResized(false);
		vk.recreateSwapChain();
		createCommandBuffers();
	}
	else if (result != VK_SUCCESS) {
		throw std::runtime_error("failed to acquire swap chain image");
	}

	updateModelUniformBuffers(imageIndex);

	VkSubmitInfo submitInfo = {};
	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

	VkSemaphore waitSemaphores[] = { vk.imageAvailableSemaphores[vk.currentFrame] };
	VkPipelineStageFlags waitStages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = waitSemaphores;
	submitInfo.pWaitDstStageMask = waitStages;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &vk.commandBuffers[imageIndex];

	VkSemaphore signalSemaphores[] = { vk.renderFinishedSemaphores[vk.currentFrame] };
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = signalSemaphores;

	vkResetFences(vk.device, 1, &vk.inFlightFences[vk.currentFrame]);

	if (vkQueueSubmit(vk.graphicsQueue, 1, &submitInfo, vk.inFlightFences[vk.currentFrame]) != VK_SUCCESS) {
		throw std::runtime_error("failed to submit draw command buffer!");
	}

	VkPresentInfoKHR presentInfo = {};
	presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = signalSemaphores;

	VkSwapchainKHR swapChains[] = { vk.swapChain };
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapChains;
	presentInfo.pImageIndices = &imageIndex;

	presentInfo.pResults = nullptr;

	vkQueuePresentKHR(vk.presentQueue, &presentInfo);


	vk.currentFrame = (vk.currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}

void VulkanRenderer::endFrame() {

}

/*void VulkanRenderer::updateUniformBuffer(uint32_t currentImage, bool ubo1) {
	static auto startTime = std::chrono::high_resolution_clock::now();

	auto currentTime = std::chrono::high_resolution_clock::now();
	float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

	UniformBufferObject ubo = {};
	ubo.model = glm::rotate(glm::mat4(1.0f), time * glm::radians(10.0f), glm::vec3(0.0f, 0.0f, 1.0f));
	ubo.view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 00.0f, 1.0f));
	ubo.proj = glm::perspective(glm::radians(45.0f), vk.swapChainExtent.width / (float)vk.swapChainExtent.height, 0.1f, 10.0f);
	ubo.proj[1][1] *= -1;

	void* data;
	vkMapMemory(vk.device, vk.uniformBuffersMemory[currentImage], 0, sizeof(ubo), 0, &data);
	memcpy(data, &ubo, sizeof(ubo));
	vkUnmapMemory(vk.device, vk.uniformBuffersMemory[currentImage]);
}*/

void VulkanRenderer::updateModelUniformBuffers(uint32_t currentImage) {
	static auto startTime = std::chrono::high_resolution_clock::now();

	auto currentTime = std::chrono::high_resolution_clock::now();

	float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

	UniformBufferObject ubo = {};
	ubo.view = camera.buildViewMatrix();
	ubo.proj = camera.buildProjectonMatrix(vk.swapChainExtent.width / (float)vk.swapChainExtent.height);

	root->update(currentImage, ubo);
}

void VulkanRenderer::createCommandBuffers() {
	vk.commandBuffers.resize(vk.swapChainFramebuffers.size());

	VkCommandBufferAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	allocInfo.commandPool = vk.commandPool;
	allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	allocInfo.commandBufferCount = (uint32_t)vk.commandBuffers.size();

	if (vkAllocateCommandBuffers(vk.device, &allocInfo, vk.commandBuffers.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate command buffers!");
	}

	for (size_t i = 0; i < vk.commandBuffers.size(); ++i) {
		{
			VkCommandBufferBeginInfo beginInfo = {};
			beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
			beginInfo.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
			beginInfo.pInheritanceInfo = nullptr;

			if (vkBeginCommandBuffer(vk.commandBuffers[i], &beginInfo) != VK_SUCCESS) {
				throw std::runtime_error("failed to begin recording command buffer!");
			}

			VkRenderPassBeginInfo renderPassInfo = {};
			renderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
			renderPassInfo.renderPass = vk.renderPass;
			renderPassInfo.framebuffer = vk.swapChainFramebuffers[i];
			renderPassInfo.renderArea.offset = { 0, 0 };
			renderPassInfo.renderArea.extent = vk.swapChainExtent;

			std::array<VkClearValue, 2> clearValues = {};
			clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
			clearValues[1].depthStencil = { 1.0f, 0 };

			renderPassInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
			renderPassInfo.pClearValues = clearValues.data();


			vkCmdBeginRenderPass(vk.commandBuffers[i], &renderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

			vkCmdBindPipeline(vk.commandBuffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, vk.graphicsPipeline);

			buildDrawStep(root, i);

			vkCmdEndRenderPass(vk.commandBuffers[i]);

			if (vkEndCommandBuffer(vk.commandBuffers[i]) != VK_SUCCESS) {
				throw std::runtime_error("failed to record command buffer!");
			}
		}
	}
}

void VulkanRenderer::buildDrawStep(std::shared_ptr<SceneNode> node, int commandBufferIndex) {
	if (node->getModel() && node->getTexture()) {
		VkBuffer vertexBuffers[] = { node->getModel()->getVertexBuffer() };
		VkDeviceSize offsets[] = { 0 };
		vkCmdBindVertexBuffers(vk.commandBuffers[commandBufferIndex], 0, 1, vertexBuffers, offsets);
		vkCmdBindIndexBuffer(vk.commandBuffers[commandBufferIndex], node->getModel()->getIndexBuffer(), 0, VK_INDEX_TYPE_UINT32);
		vkCmdBindDescriptorSets(vk.commandBuffers[commandBufferIndex], VK_PIPELINE_BIND_POINT_GRAPHICS, vk.pipelineLayout, 0, 1, &node->getDescriptorSets()[commandBufferIndex], 0, nullptr);
		vkCmdDrawIndexed(vk.commandBuffers[commandBufferIndex], static_cast<uint32_t>(node->getModel()->getIndices().size()), 1, 0, 0, 0);
	}

	for (std::vector<std::shared_ptr<SceneNode>>::const_iterator i = node->GetChildIteratorStart(); i != node->GetChildIteratorEnd(); ++i) {
		buildDrawStep((*i), commandBufferIndex);
	}
}

void VulkanRenderer::buildDescriptorData(std::shared_ptr<SceneNode> node) {
	node->createUniformBuffer();
	node->createDescriptorPool();
	node->createDescriptorSets();

	for (std::vector<std::shared_ptr<SceneNode>>::const_iterator i = node->GetChildIteratorStart(); i != node->GetChildIteratorEnd(); ++i) {
		buildDescriptorData((*i));
	}
}

void VulkanRenderer::createSyncObjects() {
	vk.imageAvailableSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
	vk.renderFinishedSemaphores.resize(MAX_FRAMES_IN_FLIGHT);
	vk.inFlightFences.resize(MAX_FRAMES_IN_FLIGHT);

	VkSemaphoreCreateInfo semaphoreInfo = {};
	semaphoreInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VkFenceCreateInfo fenceInfo = {};
	fenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	fenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	for (size_t i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i) {
		if (vkCreateSemaphore(vk.device, &semaphoreInfo, nullptr, &vk.imageAvailableSemaphores[i]) != VK_SUCCESS ||
			vkCreateSemaphore(vk.device, &semaphoreInfo, nullptr, &vk.renderFinishedSemaphores[i]) != VK_SUCCESS ||
			vkCreateFence(vk.device, &fenceInfo, nullptr, &vk.inFlightFences[i]) != VK_SUCCESS) {
			throw std::runtime_error("failed to create synchronization objects for a frame!");
		}
	}
}