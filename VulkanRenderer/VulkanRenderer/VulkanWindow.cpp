#include "pch.h"
#include "VulkanWindow.h"


VulkanWindow::VulkanWindow(unsigned int width, unsigned int height, std::string title, keyCallbackFunction keyCallback) {
	glfwInit();
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);


	window = std::shared_ptr<GLFWwindow>(glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr),
										 [=](GLFWwindow* w) {
		glfwDestroyWindow(w);
		glfwTerminate();
	});

	glfwSetWindowUserPointer(window.get(), this);
	glfwSetFramebufferSizeCallback(window.get(), framebufferResizeCallback);
	glfwSetKeyCallback(window.get(), (GLFWkeyfun)keyCallback);
}


VulkanWindow::~VulkanWindow() {
}

void VulkanWindow::pollEvents() {
	glfwPollEvents();

	if (glfwWindowShouldClose(window.get())) {
		shouldClose = true;
	}
}

glm::vec2 VulkanWindow::getWindowSize() {
	int width, height;
	glfwGetFramebufferSize(window.get(), &width, &height);

	return glm::vec2(width, height);
}

bool VulkanWindow::createSurface(VkInstance& instance, VkSurfaceKHR& surface) {
	if (glfwCreateWindowSurface(instance, window.get(), nullptr, &surface) != VK_SUCCESS) {
		return false;
	}
	return true;
}