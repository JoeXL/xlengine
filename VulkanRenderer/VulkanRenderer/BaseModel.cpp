#include "pch.h"
#include "BaseModel.h"

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>


BaseModel::BaseModel() {
}


BaseModel::~BaseModel() {
}

void BaseModel::loadFile(const std::string& filename, glm::vec3 scale) {
	tinyobj::attrib_t attrib;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string warn, err;

	std::string filepath = XLEngine::Assets::MODEL_PATH + filename;

	if (!tinyobj::LoadObj(&attrib, &shapes, &materials, &warn, &err, filepath.c_str())) {
		throw std::runtime_error(warn + err);
	}

	std::unordered_map<BaseVertex, uint32_t> uniqueVertices = {};

	for (const auto& shape : shapes) {
		for (const auto& index : shape.mesh.indices) {
			BaseVertex vertex = {};

			vertex.pos = {
					attrib.vertices[3 * index.vertex_index + 0] * scale.x,
					attrib.vertices[3 * index.vertex_index + 1] * scale.y,
					attrib.vertices[3 * index.vertex_index + 2] * scale.z
			};

			vertex.texCoord = {
				attrib.texcoords[2 * index.texcoord_index + 0],
				1.0f - attrib.texcoords[2 * index.texcoord_index + 1]
			};

			vertex.color = { 1.0f, 1.0f, 1.0f };

			if (uniqueVertices.count(vertex) == 0) {
				uniqueVertices[vertex] = static_cast<uint32_t>(vertices.size());
				vertices.push_back(vertex);
			}

			indices.push_back(uniqueVertices[vertex]);
		}
	}
}