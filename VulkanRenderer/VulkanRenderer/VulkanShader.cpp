#include "pch.h"
#include "VulkanShader.h"


VulkanShader::VulkanShader() {
}


VulkanShader::~VulkanShader() {
}

void VulkanShader::readFile(const std::string& filename, ShaderType type) {
	std::ifstream file(XLEngine::Assets::SHADER_PATH + filename, std::ios::ate | std::ios::binary);

	if (!file.is_open()) {
		throw std::runtime_error("failed to open file!");
	}

	size_t fileSize = (size_t)file.tellg();
	std::vector<char> buffer(fileSize);

	file.seekg(0);
	file.read(buffer.data(), fileSize);

	file.close();

	switch (type) {
	case VERTEX: { vertShaderCode = buffer; } break;
	case FRAGMENT: { fragShaderCode = buffer; } break;
	}
}