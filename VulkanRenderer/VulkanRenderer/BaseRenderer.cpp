#include "pch.h"
#include "BaseRenderer.h"


BaseRenderer::BaseRenderer(BaseWindow& w) : window(w){
}


BaseRenderer::~BaseRenderer() {
}

void BaseRenderer::render() {
	beginFrame();
	renderFrame();
	endFrame();

	window.pollEvents();

	if (window.shouldClose) {
		shouldClose = true;
	}
}