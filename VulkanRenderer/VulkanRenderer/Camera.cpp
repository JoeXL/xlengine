#include "pch.h"
#include "Camera.h"


Camera::Camera() {
	pitch = 0.0f;
	yaw = 0.0f;

	fov = 45.0f;
	nearPlane = 0.1f;
	farPlane = 100.0f;

	position = glm::vec3(2, 2, 2);
	worldUp = glm::vec3(0, 1, 0);

	movementSpeed = 1.0f;
	mouseSensitivity = 0.3f;

	updateCameraVectors();
}

Camera::Camera(float pitch, float yaw, const glm::vec3& position) {
	this->pitch = pitch;
	this->yaw = yaw;
	this->position = position;

	fov = 45.0f;
	nearPlane = 0.1f;
	farPlane = 10.0f;
}


Camera::~Camera() {
}

glm::mat4 Camera::buildViewMatrix() const {
	return glm::lookAt(position, position + front, up);
}

glm::mat4 Camera::buildProjectonMatrix(float currentAspect) const {
	glm::mat4 proj = glm::perspective(glm::radians(fov), currentAspect, nearPlane, farPlane);
	proj[1][1] *= -1;
	return proj;
}

void Camera::processMovement(CameraMovement direction, float dt) {
	float velocity = movementSpeed * dt;
	if (direction == FORWARD)
		position += front * velocity;
	if (direction == BACKWARD)
		position -= front * velocity;
	if (direction == LEFT)
		position -= right * velocity;
	if (direction == RIGHT)
		position += right * velocity;
	if (direction == UP)
		position += worldUp * velocity;
	if (direction == DOWN)
		position -= worldUp * velocity;
}

void Camera::processRotation(float xOffset, float yOffset) {
	xOffset *= mouseSensitivity;
	yOffset *= mouseSensitivity;

	yaw += xOffset;
	pitch += yOffset;

	if (pitch > 89.0f)
		pitch = 89.0f;
	if (pitch < -89.0f)
		pitch = -89.0f;

	updateCameraVectors();
}

void Camera::updateCameraVectors() {
	glm::vec3 front;
	front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
	front.y = sin(glm::radians(pitch));
	front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
	this->front = glm::normalize(front);
	
	right = glm::normalize(glm::cross(this->front, worldUp));
	up = glm::normalize(glm::cross(right, this->front));
}