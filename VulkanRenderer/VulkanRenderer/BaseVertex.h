#pragma once
class BaseVertex {
public:
	BaseVertex();
	virtual ~BaseVertex();

	glm::vec3 pos;
	glm::vec3 color;
	glm::vec2 texCoord;
	glm::vec3 normal;
	glm::vec3 tangent;

	bool operator==(const BaseVertex& other) const {
		return pos == other.pos && color == other.color && texCoord == other.texCoord;
	}
};

namespace std {
	template<> struct hash<BaseVertex> {
		size_t operator()(BaseVertex const& vertex) const {
			return ((hash<glm::vec3>()(vertex.pos) ^
				(hash<glm::vec3>()(vertex.color) << 1)) >> 1) ^
					 (hash<glm::vec2>()(vertex.texCoord) << 1);
		}
	};
}