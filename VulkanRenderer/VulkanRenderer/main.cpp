#include "pch.h"
#include <iostream>
#include "VulkanRenderer.h"
#include "VulkanModel.h"

Camera* camera;
GLFWwindow* vulWin = nullptr;
float lastX = 1280/2;
float lastY = 720/2;

void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods) {
	/*if (key == GLFW_KEY_W && (action == GLFW_PRESS || GLFW_REPEAT)) {
		camera->processMovement(CameraMovement::FORWARD, 0.1);
	}
	if (key == GLFW_KEY_S && (action == GLFW_PRESS || GLFW_REPEAT)) {
		camera->processMovement(CameraMovement::BACKWARD, 0.1);
	}
	if (key == GLFW_KEY_A && (action == GLFW_PRESS || GLFW_REPEAT)) {
		camera->processMovement(CameraMovement::LEFT, 0.1);
	}
	if (key == GLFW_KEY_D && (action == GLFW_PRESS || GLFW_REPEAT)) {
		camera->processMovement(CameraMovement::RIGHT, 0.1);
	}*/
	vulWin = window;
}

void cursorPositionCallback(GLFWwindow* window) {
	if (vulWin) {
		glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
		double xPos, yPos;
		glfwGetCursorPos(window, &xPos, &yPos);

		float xOffset = xPos - lastX;
		float yOffset = lastY - yPos;
		lastX = xPos;
		lastY = yPos;

		camera->processRotation(xOffset, yOffset);
	}
}

void processInput(GLFWwindow* window) {
	float cameraSpeed = 0.005;
	if (window) {
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
			camera->processMovement(CameraMovement::FORWARD, cameraSpeed);
		}
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
			camera->processMovement(CameraMovement::BACKWARD, cameraSpeed);
		}
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
			camera->processMovement(CameraMovement::LEFT, cameraSpeed);
		}
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
			camera->processMovement(CameraMovement::RIGHT, cameraSpeed);
		}
		if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
			camera->processMovement(CameraMovement::UP, cameraSpeed);
		}
		if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS) {
			camera->processMovement(CameraMovement::DOWN, cameraSpeed);
		}
	}
}

int main() {

	VulkanWindow window(1280, 720, "Vulkan!", &keyCallback);
	VulkanRenderer renderer(window);

	camera = &renderer.getCamera();
	camera->setPosition(glm::vec3(0, 1, -10));
	camera->setPitch(0);
	camera->setYaw(90.0f);

	renderer.init();

#ifdef _DEBUG
	renderer.addModel("cube.obj", "chaletMod", glm::vec3(5,5,5));
	renderer.addModel("cube.obj", "cubeMod");
	renderer.addModel("cube.obj", "dogMod", glm::vec3(0.015, 0.015, 0.015));
	renderer.addModel("cube.obj", "2bMod", glm::vec3(0.01, 0.01, 0.01));
#else
	renderer.addModel("chalet.obj", "chaletMod", glm::vec3(5, 5, 5));
	renderer.addModel("cube.obj", "cubeMod");
	renderer.addModel("dog.obj", "dogMod", glm::vec3(0.015, 0.015, 0.015));
	renderer.addModel("2b.obj", "2bMod", glm::vec3(0.01, 0.01, 0.01));
#endif

	renderer.addTexture("chalet.jpg", "chaletTex");
	renderer.addTexture("texture.jpg", "cubeTex");
	renderer.addTexture("grass.jpg", "grassTex");
	renderer.addTexture("dog.jpg", "dogTex");
	renderer.addTexture("2b.png", "2bTex");
	renderer.addTexture("floor.jpg", "floorTex");
	renderer.addTexture("wall.jpg", "wallTex");
	std::shared_ptr<SceneNode> root = std::make_shared<SceneNode>(renderer.getContext());

	{ // Room
		int floorCount = 20;
		float floorSize = 2;
		for (int i = 0; i < floorCount; ++i) {
			for (int j = 0; j < floorCount; ++j) {
				std::shared_ptr<SceneNode> floor = std::make_shared<SceneNode>(renderer.getContext());
				floor->setModel(renderer.getModel("cubeMod"));
				floor->setTexture(renderer.getTexture("floorTex"));
				floor->setTransform(glm::translate(floor->getTransform(), glm::vec3(((-floorCount * floorSize) / 2.0) + (i * floorSize), 0, ((-floorCount * floorSize) / 2.0) + (j * floorSize))));
				floor->setTransform(glm::scale(floor->getTransform(), glm::vec3(floorSize, 1, floorSize)));
				root->addChild(floor);
			}
		}

		//Wall
		float wallDistance = (floorCount * floorSize) / 2.0;
		float wallHeightCount = 5;
		float wallSize = 4;
		glm::vec3 wallPos[4] = {
			glm::vec3(wallDistance, 0, 0),
			glm::vec3(-wallDistance, 0,  0),
			glm::vec3(0, 0, wallDistance),
			glm::vec3(0, 0, -wallDistance)
		};
		glm::vec3 wallRot[4] = {
			glm::vec3(0,1,0),
			glm::vec3(0,1,0),
			glm::vec3(0,0,0),
			glm::vec3(0,0,0)
		};
		for (int wallIt = 0; wallIt < 4; ++wallIt) {
			std::shared_ptr<SceneNode> wall = std::make_shared<SceneNode>(renderer.getContext());
			for (int i = 0; i < floorCount; ++i) {
				for (int j = 0; j < wallHeightCount; ++j) {
					std::shared_ptr<SceneNode> wallBlock = std::make_shared<SceneNode>(renderer.getContext());
					wallBlock->setModel(renderer.getModel("cubeMod"));
					wallBlock->setTexture(renderer.getTexture("wallTex"));
					wallBlock->setTransform(glm::translate(wallBlock->getTransform(), glm::vec3(((-floorCount * wallSize) / 2.0) + (i * wallSize), (j * wallSize), 0)));
					wallBlock->setTransform(glm::scale(wallBlock->getTransform(), glm::vec3(wallSize, wallSize, wallSize)));
					wall->addChild(wallBlock);
				}
			}
			wall->setTransform(glm::translate(wall->getTransform(), wallPos[wallIt]));
			if (wallRot[wallIt].y != 0) {
				wall->setTransform(glm::rotate(wall->getTransform(), (float)glm::radians(90.0f), wallRot[wallIt]));
			}

			root->addChild(wall);
		}
	}

	{ // Chalet
		std::shared_ptr<SceneNode> chalet = std::make_shared<SceneNode>(renderer.getContext());
		chalet->setModel(renderer.getModel("chaletMod"));
		chalet->setTexture(renderer.getTexture("chaletTex"));
		chalet->setTransform(glm::translate(chalet->getTransform(), glm::vec3(-8, 0.5, 0)));
		chalet->setTransform(glm::rotate(chalet->getTransform(), (float)glm::radians(90.0f), glm::vec3(-1, 0, 0)));
		chalet->setTransform(glm::rotate(chalet->getTransform(), (float)glm::radians(90.0f), glm::vec3(0, 0, -1)));
		root->addChild(chalet);
	}

	std::shared_ptr<SceneNode> dog = std::make_shared<SceneNode>(renderer.getContext());
	{ // Dog
		dog->setModel(renderer.getModel("dogMod"));
		dog->setTexture(renderer.getTexture("dogTex"));
		dog->setTransform(glm::translate(dog->getTransform(), glm::vec3(0, 0.5, 0)));
		dog->setTransform(glm::rotate(dog->getTransform(), (float)glm::radians(90.0f), glm::vec3(-1, 0, 0)));
		dog->setTransform(glm::rotate(dog->getTransform(), (float)glm::radians(180.0f), glm::vec3(0, 0, 1)));
		root->addChild(dog);
	}

	{ // 2B
		std::shared_ptr<SceneNode> twoB = std::make_shared<SceneNode>(renderer.getContext());
		twoB->setModel(renderer.getModel("2bMod"));
		twoB->setTexture(renderer.getTexture("2bTex"));
		twoB->setTransform(glm::translate(twoB->getTransform(), glm::vec3(3, 0.65, 0)));
		twoB->setTransform(glm::rotate(twoB->getTransform(), (float)glm::radians(220.0f), glm::vec3(0, 1, 0)));
		root->addChild(twoB);
	}

	renderer.setSceneRoot(root);

	renderer.buildDrawSteps();

	while (true) {
		processInput(vulWin);
		cursorPositionCallback(vulWin);
		renderer.render();
		dog->setTransform(glm::rotate(dog->getTransform(), (float)glm::radians(0.05f), glm::vec3(0, 0, 1)));
		if (renderer.shouldClose) break;
	}

	return 0;
}