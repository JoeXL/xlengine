#pragma once
#include "SceneNode.h"
#include "BaseRenderer.h"
#include "VulkanWindow.h"
#include "VulkanShader.h"
#include "VulkanModel.h"
#include "VulkanTexture.h"

#include "VulkanDebugEXT.h"
#include "VulkanQueueFamily.h"

#include "Camera.h"

const std::string MODEL_PATH = "cube.obj";
const std::string TEXTURE_PATH = XLEngine::Assets::TEXTURE_PATH + "chalet.jpg";

class VulkanRenderer :
	public BaseRenderer {
public:
	VulkanRenderer(VulkanWindow& w);
	virtual  ~VulkanRenderer();

	virtual void init() {
		vk.initVulkan();
	}

	void addModel(std::string filename, std::string id, glm::vec3 scale = glm::vec3(1,1,1)) {
		std::shared_ptr<VulkanModel> model = std::make_shared<VulkanModel>(vk);
		model->id = id;
		model->loadFile(filename, scale);
		model->uploadToGPU();

		models.push_back(model);
	}

	void addTexture(std::string filename, std::string id) {
		std::shared_ptr<VulkanTexture> tex = std::make_shared<VulkanTexture>(vk);
		tex->id = id;
		tex->loadFile(filename);

		textures.push_back(tex);
	}

	std::shared_ptr<VulkanModel> getModel(std::string id) {
		for (int i = 0; i < models.size(); ++i) {
			if (models.at(i)->id == id) {
				return models.at(i);
			}
		}
		return models.at(0);
	}

	std::shared_ptr<VulkanTexture> getTexture(std::string id) {
		for (int i = 0; i < textures.size(); ++i) {
			if (textures.at(i)->id == id) {
				return textures.at(i);
			}
		}
		return textures.at(0);
	}

	void setSceneRoot(std::shared_ptr<SceneNode> root) {
		this->root = root;
	}

	void buildDrawSteps() {
		buildDescriptorData(root);
		createCommandBuffers();
		createSyncObjects();
	}

	Camera& getCamera() { return camera; }

	XLVulkanContext& getContext() { return vk; }

protected:
	virtual void beginFrame() override;
	virtual void renderFrame() override;
	virtual void endFrame() override;

	void updateModelUniformBuffers(uint32_t currentImage);

	void createCommandBuffers();
	void createSyncObjects();

	VulkanShader basicShader;

	std::shared_ptr<SceneNode> root;

	Camera camera;

	void buildDescriptorData(std::shared_ptr<SceneNode> node);
	void buildDrawStep(std::shared_ptr<SceneNode> node, int commandBufferIndex);

	std::vector<std::shared_ptr<VulkanModel>> models;
	std::vector<std::shared_ptr<VulkanTexture>> textures;

	XLVulkanContext vk;
};

