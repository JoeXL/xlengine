#pragma once

#include "VulkanModel.h"
#include "VulkanTexture.h"

#include "XLVulkanContext.h"

class SceneNode {
public:
	SceneNode(XLVulkanContext& context);
	~SceneNode();

	const glm::mat4& getTransform() const { return transform; }
	glm::mat4 getWorldTransform() const { return worldTransform; }
	void setTransform(const glm::mat4& matrix) { transform = matrix; }

	const glm::vec4& getColor() const { return color; }
	void setColor(const glm::vec4& col) { color = col; }

	const glm::vec3& getModelScale() const { return modelScale; }
	void setModelScale(const glm::vec3& scl) { modelScale = scl; }

	const std::shared_ptr<VulkanModel> getModel() const { return model; }
	void setModel(const std::shared_ptr<VulkanModel> mod) { model = mod; }
	void setModel(VulkanModel& mod) { model = std::shared_ptr<VulkanModel>(&mod); }

	const std::shared_ptr<VulkanTexture> getTexture() const { return texture; }
	void setTexture(const std::shared_ptr<VulkanTexture> tex) { texture = tex; }
	void setTexture(VulkanTexture& tex) { texture = std::shared_ptr<VulkanTexture>(&tex); }

	void addChild(const std::shared_ptr<SceneNode> child);
	void removeChild(const std::shared_ptr<SceneNode> child);

	virtual void update(uint32_t currentImage, UniformBufferObject ubo);

	float getBoundingRadius() const { return boundingRadius; }
	void setBoundingRadius(float f) { boundingRadius = f; }

	float getCameraDistance() const { return distanceFromCamera; }

	static bool compareByCameraDistance(const std::shared_ptr<SceneNode>& a, const std::shared_ptr<SceneNode>& b) {
		return (a->distanceFromCamera < b->distanceFromCamera) ? true : false;
	}

	void createDescriptorPool();
	void createDescriptorSets();

	std::vector<VkDescriptorSet>& getDescriptorSets() { return descriptorSets; }

	void createUniformBuffer();
	void updateUniformBuffer(uint32_t currentImage, UniformBufferObject ubo);

	void cleanup() {
		if (model && texture) {
			vkDestroyDescriptorPool(vk.device, descriptorPool, nullptr);

			for (size_t i = 0; i < vk.swapChainImages.size(); i++) {
				vkDestroyBuffer(vk.device, uniformBuffers[i], nullptr);
				vkFreeMemory(vk.device, uniformBuffersMemory[i], nullptr);
			}

			model = nullptr;
			texture = nullptr;
		}
		
		for (std::vector<std::shared_ptr<SceneNode>>::iterator i = children.begin(); i != children.end(); ++i) {
			(*i)->cleanup();
		}
	}

	std::vector< std::shared_ptr<SceneNode>>::const_iterator GetChildIteratorStart() { return children.begin(); }
	std::vector< std::shared_ptr<SceneNode>>::const_iterator GetChildIteratorEnd() { return children.end(); }

protected:
	std::shared_ptr<SceneNode> parent;
	std::shared_ptr<VulkanModel> model;
	std::shared_ptr<VulkanTexture> texture;
	//std::shared_ptr<BaseTexture> bumpMap;
	glm::mat4 worldTransform;
	glm::mat4 transform;
	glm::vec3 modelScale;
	glm::vec4 color;
	float distanceFromCamera;
	float boundingRadius;
	std::vector<std::shared_ptr<SceneNode>> children;

	VkDescriptorPool descriptorPool;
	std::vector<VkDescriptorSet> descriptorSets;

	XLVulkanContext& vk;

	std::vector<VkBuffer> uniformBuffers;
	std::vector<VkDeviceMemory> uniformBuffersMemory;

	//friend class XLVulkanContext;
};

