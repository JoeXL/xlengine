#pragma once
#include "BaseWindow.h"

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

typedef void(*keyCallbackFunction)(GLFWwindow* window, int key, int scancode, int action, int mods);

class VulkanWindow :
	public BaseWindow {
public:
	VulkanWindow(unsigned int width, unsigned int height, std::string title, keyCallbackFunction keyCallback);
	virtual ~VulkanWindow();

	virtual void pollEvents() override;

	virtual glm::vec2 getWindowSize() override;

	bool createSurface(VkInstance& instance, VkSurfaceKHR& surface);

protected:
	std::shared_ptr<GLFWwindow> window;

	static void framebufferResizeCallback(GLFWwindow* window, int width, int height) {
		auto app = reinterpret_cast<VulkanWindow*>(glfwGetWindowUserPointer(window));
		app->framebufferResized = true;
	}
};

