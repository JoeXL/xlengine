#pragma once
#include "BaseShader.h"

class VulkanShader :
	public BaseShader {
public:
	VulkanShader();
	virtual ~VulkanShader();

	virtual void readFile(const std::string& filename, ShaderType type);

	const std::vector<char>& getVertexCode() { return vertShaderCode; }
	const std::vector<char>& getFragmentCode() { return fragShaderCode; }

protected:
	std::vector<char> vertShaderCode;
	std::vector<char> fragShaderCode;
};

