#pragma once
#include "BaseModel.h"

#include "XLVulkanContext.h"

class VulkanModel :
	public BaseModel {
public:
	VulkanModel(XLVulkanContext& context);
	virtual ~VulkanModel();

	void uploadToGPU() override;

	VkBuffer& getVertexBuffer() { return vertexBuffer; }
	VkBuffer& getIndexBuffer() { return indexBuffer; }

	std::string id;

protected:
	void createVertexBuffer();
	void createIndexBuffer();

	//vertexBuffer, vertexBufferMemory, indexBuffer, indexBufferMemory

	VkBuffer vertexBuffer;
	VkDeviceMemory vertexBufferMemory;
	VkBuffer indexBuffer;
	VkDeviceMemory indexBufferMemory;

	XLVulkanContext& vk;

	friend class XLVulkanContext;
};

