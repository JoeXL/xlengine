#include "pch.h"
#include "SceneNode.h"


SceneNode::SceneNode(XLVulkanContext& context) : vk(context) {
	transform = glm::mat4(1.0);
}


SceneNode::~SceneNode() {
	children.clear();

	descriptorSets.clear();
	uniformBuffers.clear();
	uniformBuffersMemory.clear();
}

void SceneNode::addChild(const std::shared_ptr<SceneNode> child) {
	if (child.get() != this) {
		children.push_back(child);
		child->parent = std::shared_ptr<SceneNode>(this);
	}
}

void SceneNode::removeChild(const std::shared_ptr<SceneNode> child) {
	if (child.get() != this) {
		for (std::vector<std::shared_ptr<SceneNode>>::iterator i = children.begin(); i != children.end(); ++i) {
			if ((*i) == child) {
				i = children.erase(i);
				--i;
			}
		}
	}
}

void SceneNode::update(uint32_t currentImage, UniformBufferObject ubo) {
	if (parent) {
		worldTransform = parent->worldTransform * transform;
	}
	else {
		worldTransform = transform;
	}

	for (std::vector<std::shared_ptr<SceneNode>>::iterator i = children.begin(); i != children.end(); ++i) {
		(*i)->update(currentImage, ubo);
	}


	ubo.model = worldTransform;

	updateUniformBuffer(currentImage, ubo);
}

void SceneNode::createDescriptorPool() {
	if (!model || !texture) {
		return;
	}
	std::array<VkDescriptorPoolSize, 2> poolSizes = {};
	poolSizes[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	poolSizes[0].descriptorCount = static_cast<uint32_t>(vk.swapChainImages.size());
	poolSizes[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	poolSizes[1].descriptorCount = static_cast<uint32_t>(vk.swapChainImages.size());

	VkDescriptorPoolCreateInfo poolInfo = {};
	poolInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = static_cast<uint32_t>(vk.swapChainImages.size());

	if (vkCreateDescriptorPool(vk.device, &poolInfo, nullptr, &descriptorPool) != VK_SUCCESS) {
		throw std::runtime_error("failed to create descriptor pool!");
	}
}

void SceneNode::createDescriptorSets() {
	if (!model || !texture) {
		return;
	}
	std::vector<VkDescriptorSetLayout> layouts(vk.swapChainImages.size(), vk.descriptorSetLayout);
	VkDescriptorSetAllocateInfo allocInfo = {};
	allocInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	allocInfo.descriptorPool = descriptorPool;
	allocInfo.descriptorSetCount = static_cast<uint32_t>(vk.swapChainImages.size());
	allocInfo.pSetLayouts = layouts.data();

	descriptorSets.resize(vk.swapChainImages.size());

	if (vkAllocateDescriptorSets(vk.device, &allocInfo, descriptorSets.data()) != VK_SUCCESS) {
		throw std::runtime_error("failed to allocate descriptor sets!");
	}

	for (size_t i = 0; i < vk.swapChainImages.size(); i++) {
		VkDescriptorBufferInfo bufferInfo = {};
		bufferInfo.buffer = uniformBuffers[i];
		bufferInfo.offset = 0;
		bufferInfo.range = sizeof(UniformBufferObject);

		VkDescriptorImageInfo imageInfo = {};
		imageInfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		imageInfo.imageView = texture->getTextureImageView();
		imageInfo.sampler = texture->getTextureSampler();

		std::array<VkWriteDescriptorSet, 2> descriptorWrites = {};

		descriptorWrites[0].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptorWrites[0].dstSet = descriptorSets[i];
		descriptorWrites[0].dstArrayElement = 0;
		descriptorWrites[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		descriptorWrites[0].descriptorCount = 1;
		descriptorWrites[0].pBufferInfo = &bufferInfo;

		descriptorWrites[1].sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
		descriptorWrites[1].dstSet = descriptorSets[i];
		descriptorWrites[1].dstBinding = 1;
		descriptorWrites[1].dstArrayElement = 0;
		descriptorWrites[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		descriptorWrites[1].descriptorCount = 1;
		descriptorWrites[1].pImageInfo = &imageInfo;

		vkUpdateDescriptorSets(vk.device, static_cast<uint32_t>(descriptorWrites.size()), descriptorWrites.data(),
							   0, nullptr);
	}
}

void SceneNode::createUniformBuffer() {
	if (!model || !texture) {
		return;
	}
	VkDeviceSize bufferSize = sizeof(UniformBufferObject);

	uniformBuffers.resize(vk.swapChainImages.size());
	uniformBuffersMemory.resize(vk.swapChainImages.size());

	for (size_t i = 0; i < vk.swapChainImages.size(); i++) {
		vk.createBuffer(bufferSize, VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT
					 | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, uniformBuffers[i], uniformBuffersMemory[i]);
	}

}

void SceneNode::updateUniformBuffer(uint32_t currentImage, UniformBufferObject ubo) {
	if (model && texture) {
		void* data;
		vkMapMemory(vk.device, uniformBuffersMemory[currentImage], 0, sizeof(ubo), 0, &data);
		memcpy(data, &ubo, sizeof(ubo));
		vkUnmapMemory(vk.device, uniformBuffersMemory[currentImage]);
	}
}