#pragma once
class BaseWindow {
public:
	BaseWindow();
	virtual ~BaseWindow();

	virtual void pollEvents() = 0;

	bool shouldClose = false;

	virtual glm::vec2 getWindowSize() = 0;

	bool hasResized() const { return framebufferResized; }
	void setResized(bool b) { framebufferResized = b; }

protected:
	bool framebufferResized = false;
};

